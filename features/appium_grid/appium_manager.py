import shlex
import sys
import platform
import subprocess
import logging
from threading import Thread
from Queue import Queue, Empty

__author__ = 'searcher'

from features.appium_grid.available_ports import AvailablePorts
from features.appium_grid.command_prompt import CommandPrompt
from features.globvar import *

ON_POSIX = 'posix' in sys.builtin_module_names
logging.basicConfig(level=logging.INFO,
                    format="[%(levelname)-8s %(asctime)s] %(message)s")
logger = logging.getLogger(__name__)


class AppiumManager:
    cmd = CommandPrompt()
    ap = AvailablePorts()

    def __init__(self):
        pass

    def enqueue_output(self, out, queue):
        for line in iter(out.readline, b''):
            queue.put(line)
        out.close()

    #
    #  start appium with default arguments
    #
    def start_default_appium(self):
        self.cmd.run_command("appium --session-override")
        time.sleep(THINK_TIME)

    # def start_appium(self):
    def start_appium(self, port=None, chrome_port=None, bootstrap_port=None):
        if (port is not None) and (chrome_port is not None) and (bootstrap_port is not None):
            #
            #  * start appium with modified arguments :
            #  * appium port, chrome port, and bootstap port as user pass port number
            #  * @param appium port
            #  * @param chrome port
            #  * @param bootstrap port
            #
            command = "appium --session-override -p " + str(port) + " --chromedriver-port " + str(chrome_port) + " -bp " \
                      + str(bootstrap_port)
            print(command)
            output = self.cmd.run_command(command)
            print(output)
            return port
        else:
            #  start appium with auto generated ports : appium port, chrome port, and bootstap port
            # start appium server
            port = self.ap.get_available_port()
            chrome_port = self.ap.get_available_port()
            bootstrap_port = self.ap.get_available_port()
            command = "appium --session-override -p " + str(port) + " --chromedriver-port " + str(chrome_port) + " -bp " \
                      + str(bootstrap_port)
            print("Launching Appium server with the command:\n {}".format(command))
            output = self.cmd.run_command_parallel(command)
            if (output == "") or (output is None):
                logger.info("Server on port {} could not be launched.".format(port))                
                sys.exit(0)
            else:
                logger.info("Launched an Appium server on port {0}. Output: \n{1}".format(port, output))
            # output = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE, bufsize=1, close_fds=ON_POSIX,
            #                           stderr=subprocess.STDOUT)
            # print ("This is the debug from appium manager launch:")
            # q = Queue()
            # t = Thread(target=self.enqueue_output, args=(output.stdout, q))
            # t.daemon = True  # thread dies with the program
            # t.start()
            # try:
            #     line = q.get_nowait()  # or q.get(timeout=.1)
            # except Empty:
            #     print('no output yet')
            # else:  # got line
            #     print (line)
            #     if "not" in line:
            #         print("\nAppium is not installed")
            #         sys.exit(0)
            return port

    def is_server_running(self, silent_mode=True, port=4723):
        # /**
        #  * Checks whether an Appium server instance is running or not.
        #  *
        #  * @param silent_mode Whether or not to log info messages. True will activate
        #  * silent mode and no messages would be logged.
        #  * @return True if an Appium server is already running, false otherwise.
        #  */
        op_sys = platform.system()
        try:
            if "Windows" in op_sys:  # .find("Windows") != -1:
                # check_server_alive_command = "echo off & FOR /F \"usebackq tokens=5\" %a in (`netstat -nao ^| findstr /R /C:\"" + str(
                #     port) + " \"`) do (FOR /F \"usebackq\" %b in (`TASKLIST /FI \"PID eq %a\" ^| findstr /I node.exe`) do @echo %a)"
                print (
                    "Unfortunatelly we don't support Windows. Please check the code of the \"is_server_running\" method of Appium_manager")
                raise
            elif "Darwin" in op_sys:
                # Using command substitution
                check_server_alive_command = "lsof -i -P | grep node | grep " + str(port)
            else:
                # Using command substitution
                check_server_alive_command = "PID=\"$(lsof -i -P | grep -f node | grep " + str(port) + ")\";echo $PID"

            if not silent_mode:
                logger.info(
                    "Checking to see if a server instance is running with the command: {0}; on port: {1}".format(
                        check_server_alive_command, port))

            time.sleep(THINK_TIME)
            is_server_alive_output, err = self.cmd.run_command_ser(check_server_alive_command)

        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise

        if (is_server_alive_output == "") or (is_server_alive_output is None) or (err is not None):
            if not silent_mode:
                logger.info("Server on port {} is not running.".format(port))
            return False
        else:
            if not silent_mode:
                logger.info("Found a running server instance with PID = {0}".format(is_server_alive_output))
            return True

    def server_pid(self, port=4723):
        pass

    def kill_appium (self, pid):
        pass
