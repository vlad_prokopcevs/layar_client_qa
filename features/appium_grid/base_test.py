import shlex
import subprocess
import time

from features.appium_grid.available_ports import AvailablePorts
from features.appium_grid.command_prompt import CommandPrompt

__author__ = 'searcher'

import sys
from multiprocessing import Pool
from subprocess import call
import logging
import argparse
from functools import partial

from features.appium_grid.device_configuration import DeviceConfiguration
from features.globvar import APK_PATH, THINK_TIME, APP_PATH, APK_NAME

logging.basicConfig(level=logging.INFO,
                    format="[%(levelname)-8s %(asctime)s] %(message)s")
logger = logging.getLogger(__name__)


class SingleDevice:
    device_number = 0
    platform_name = ""
    deviceId = ""
    deviceName = ""
    osVersion = ""
    port = ""
    app_path = ""

    def __init__(self, all_devices, i):
        device_number = (i + 1)
        self.device_number = device_number
        self.platform_name = all_devices.devices.get("platform_name" + str(device_number))
        self.deviceId = all_devices.devices.get("device_id" + str(device_number))
        self.deviceName = all_devices.devices.get("device_name" + str(device_number)).replace("\'", "").replace("\\", "")
        self.osVersion = all_devices.devices.get("os_version" + str(device_number))
        ap = AvailablePorts()
        self.port = ap.get_available_port()
        if "iOS" in self.platform_name:
            self.app_path = all_devices.app_path
        else:
            self.app_path = all_devices.apk_path
        pass

    def __str__(self):
        return "device_id={0}; device_name='{1}'; os_version={2}; port={3}; app='{4}'".format(
            self.deviceId, self.deviceName, self.osVersion, self.port, self.app_path.replace(' ', '\ '))


class AllDevices:
    def __init__(self, dev_type="andr"):
        # deviceCount = 0
        self.devices = dict({})
        # apk_path = ""
        # app_path = ""
        self.deviceConf = DeviceConfiguration()
        try:
            self.devices = self.deviceConf.get_devices(dev_type)
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise
        self.deviceCount = len(self.devices) / 4
        pass


def parse_arguments():
    parser = argparse.ArgumentParser('Run tests in parallel mode')
    parser.add_argument('-c', type=int, help='number of concurrent runs '
                                             '(default = 5)', default=5)
    parser.add_argument('--verbose', '-v', action='store_true', help='verbose '
                                                                     'output')
    parser.add_argument('--tags', '-t', help="specify behave tags to run"
                                             "(default=-skip)", default='-skip')
    parser.add_argument('--apk', '-a', help='specify path for the apk to test')
    parser.add_argument('--app', '-p', help='specify path for the app to test')

    args = parser.parse_args()
    return args


def _run_feature(feature, tags=None):
    logger.debug("Processing feature: {}".format(feature))
    params = "--tags={}".format(tags)
    cmd = "behave -f plain {0} -i {1}".format(params, feature)
    r = call(cmd, shell=True)
    status = 'ok' if r == 0 else 'failed'
    return feature, status


def uninstall_device(device):
    logger.info("Processing device: {}".format(device.deviceName))
    cmd = "adb -s {0} uninstall {1}".format(device.deviceId, APK_NAME) #"com.esvaru.popcards"
    logger.info(cmd)
    r = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    logs = ""
    while True:
        line = r.stdout.readline()
        form_line = line.strip("\n\r")
        print form_line
        if not line:
            break
        logs = logs + "\n\r" + form_line
    status = 'ok' if r.stderr is None else str(r.stderr) #'failed'
    return device, status, logs


def install_device(device):
    '''
    @param device:
    @return:
    @type device SingleDevice
    '''
    logger.info("Processing device: {}".format(device.deviceName))
    # cmd = "adb -s {0} install {1}".format(device.deviceId, "/Users/searcher/Google Drive/builds/Popcards/app-debug_v1.1.1.apk".replace(' ', '\ '))
    cmd = "adb -s {0} install {1}".format(device.deviceId, device.app_path.replace(' ', '\ '))
    logger.info(cmd)
    r = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    logs = ""
    while True:
        line = r.stdout.readline()
        form_line = line.strip("\n\r")
        print form_line
        if not line:
            break
        logs = logs + "\n\r" + form_line
    status = 'ok' if r.stderr is None else str(r.stderr) #'failed'
    return device, status, logs


def _run_device(device, tags=None):
    logger.info("Processing device: {}".format(device.deviceName))
    if tags is not None:
        params = "--tags={}".format(tags)
        cmd = "behave -f plain {0} -D device_id={1} -D device_name='{2}' -D os_version={3} -D port={4} -D app='{5}' -D delay={6} -D platformName={7}".format(
            params, device.deviceId, device.deviceName, device.osVersion, device.port,
            device.app_path.replace(' ', '\ '), str(device.device_number), device.platform_name)
    else:
        cmd = "behave -f plain -D device_id={0} -D device_name='{1}' -D os_version={2} -D port={3} -D app='{4}' -D delay={5} -D platformName={6}".format(
            device.deviceId, device.deviceName, device.osVersion, device.port, device.app_path.replace(' ', '\ '),
            str(device.device_number), device.platform_name)
    r = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    logs = ""
    while True:
        line = r.stdout.readline()
        form_line = line.strip("\n\r")
        print form_line
        if not line:
            break
        logs = logs + "\n\r" + form_line
    status = 'ok' if r.stderr is None else str(r.stderr)  # 'failed'
    return device, status, logs


def main():
    # print shlex.os.environ['PATH']
    args = parse_arguments()
    pool = Pool(args.c)
    andr_devices = AllDevices("andr")
    if args.apk:
        andr_devices.apk_path = args.apk
    else:
        andr_devices.apk_path = APK_PATH

    ios_devices = AllDevices("iOS")
    if args.app:
        ios_devices.app_path = args.app
    else:
        ios_devices.app_path = APP_PATH

    andr_device_list = []
    if andr_devices.deviceCount <= 0:
        print ("No Android devices attached.")
    else:
        for i in xrange(andr_devices.deviceCount):
            andr_device_list.append(SingleDevice(andr_devices, i))
        logger.info("Found {} Android devices".format(andr_devices.deviceCount))
        logger.info('; '.join(map(str, andr_device_list)))
        run_device = partial(_run_device, tags=args.tags)


        # this cycle can be used to install new app version on all andr_devices (Sequentially)
        # for device in andr_device_list:
        #     device_name, status, logs = uninstall_device(device)
        #     print "{0:50}: {1}\n\r Logs: {2}".format(device_name, status, logs)
        #     install_device(device)
        #     print "{0:50}: {1}\n\r Logs: {2}".format(device_name, status, logs)

        # this cycle can be used to install new app version on all andr_devices (parallel)
        for device, status, logs in pool.map(uninstall_device, andr_device_list):
            print "{0:50}: {1}\n\r {2}".format(device, status, logs)
        for device, status, logs in pool.map(install_device, andr_device_list):
            print "{0:50}: {1}\n\r {2}".format(device, status, logs)

        # this cycle runs tests in parallel on several andr_devices
        # for device, status, logs in pool.map(run_device, andr_device_list):
        #     print "{0:50}: {1}\n\r {2}".format(device, status, logs)

        # this cycle runs tests in sequence on several Android devices (for debug purposes)
        # for device in andr_device_list:
        #     run_device(device)

    # ios_device_list = []
    # if ios_devices.deviceCount <= 0:
    #     print ("No iOS devices attached.")
    # else:
    #     for i in xrange(ios_devices.deviceCount):
    #         ios_device_list.append(SingleDevice(ios_devices, i))
    #     logger.info("Found {} iOS devices".format(ios_devices.deviceCount))
    #     logger.info('; '.join(map(str, ios_device_list)))
    #
    #     run_device = partial(_run_device, tags=args.tags)
    #     # this cycle runs tests in sequence on several iOS devices (for debug purposes)
    #     for device in ios_device_list:
    #         run_device(device)

    # cmd = CommandPrompt()
    # cmd.run_command_ser("killall node")



if __name__ == '__main__':
    main()
