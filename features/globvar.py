import os
import time

# Returns abs path relative to this file and not cwd
PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)

# think times can be useful e.g. when testing with an emulator
THINK_TIME = 2

# App to test
# APK_PATH = '/Users/searcher/Google Drive/builds/Virtual Asthma/MYVirtualAsthmaCare_DEV_VPN 19.10.2015 2.apk'
# APK_PATH = '/Users/searcher/Google Drive/talkingmedicines/talkingmedicines-new-RC.apk'
# APK_PATH = '/Users/searcher/Google Drive/popcards/popcards-1.2-RC.apk'
APK_PATH = '/Users/searcher/Google Drive/Layar Testing/layar_8_4_5_20160229_25c9b35_RC3.apk'
# APK_PATH = '/Users/searcher/Google Drive/HSBC Android/beacons/hsbc-beacons-20160223-hsbc-experimental-debug.apk'
# APK_PATH = '/Users/searcher/Downloads/hsbc.apk'
# APK_NAME = "com.layar.hsbc"
APK_NAME = "com.layar"
# APK_NAME = "com.esvaru.popcards"
# APK_NAME = "com.lloyds.myvirtualasthmacare"
# APP_PATH = '/Users/searcher/Library/Developer/Xcode/DerivedData/Virtual_Pharmacy-fcyiosxhcbxdnibxbubwnpvuxanq/Build/Products/AdHoc Testing-iphonesimulator/Virtual Pharmacist.app'
APP_PATH = '/Users/searcher/Downloads/Layar Development.ipa'
# APP_PATH = '/Users/searcher/Library/Developer/Xcode/DerivedData/Layar-akxracgfbypkgrcmlsjttnuottky/Build/Products/Debug-iphoneos/Layar.app'
# APP_PATH = '/Users/searcher/Library/Developer/Xcode/DerivedData/Layar-akxracgfbypkgrcmlsjttnuottky/Build/Products/Debug-iphonesimulator/Layar.app'

# list of devices requiring small video
SMALL_DEVICES = [u'201abc4c4224a66a9a1fbefd4e73e5e8916ec99c', u'c8ce4a364babdc50ce6b65d84579b1d1231d55c0']
# list of slow devices by ID
SLOW_DEVICES = [u'4065840439d757a8a90f198e07f089728d206e57', u'9370D003-4485-482E-9602-F0C7E4960A1B']

SCREENSHOT_DIR = os.path.join(os.getcwd(), '/screenshots/')
