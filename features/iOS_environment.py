from appium import webdriver
from globvar import *
from parse import *


# TODO: Create a function, that will find out all the device IDs for every device connected
# And launch a separate appium server for each of them.
def runTest(device_ID):
    pass


def before_all(context):
    # caps for a real device

    # desired_caps = {'platformName': 'iOS',
    #                 'platformVersion': '8.2',
    #                 'deviceName': 'iPhone 5 iOS 8.2',
    #                 'udid': '191aa88133362478d19330b38941409fb0d47704',
    #                 # system_profiler SPUSBDataType | sed -n -e '/iPad/,/Serial/p' -e '/iPhone/,/Serial/p' | grep "Serial Number:" | awk -F ": " '{print $2}'
    #                 # 'app': '/Users/searcher/Library/Developer/Xcode/DerivedData/Virtual_Pharmacy-fcyiosxhcbxdnibxbubwnpvuxanq/Build/Products/AdHoc Testing-iphoneos/Virtual Pharmacist.app'
    #                 'app': '/Users/searcher/Downloads/Virtual Pharmacy.ipa'
    #                 }

    # caps for a simulator
    desired_caps = {
                    'waitForAppScript': '$.delay(1000)',
                    'autoAcceptAlerts': 'true',
                    'platformName': 'iOS',
                    'platformVersion': '8.4',
                    'deviceName': 'iPhone 5s',
                    # 'udid': '191aa88133362478d19330b38941409fb0d47704',
                    # system_profiler SPUSBDataType | sed -n -e '/iPad/,/Serial/p' -e '/iPhone/,/Serial/p' | grep "Serial Number:" | awk -F ": " '{print $2}'
                    #
                    # 'app': '/Users/searcher/Library/Developer/Xcode/DerivedData/Virtual_Pharmacy-fcyiosxhcbxdnibxbubwnpvuxanq/Build/Products/AdHoc Testing-iphoneos/Virtual Pharmacist.app'
                    'app': '/Users/searcher/Library/Developer/Xcode/DerivedData/Virtual_Pharmacy-fcyiosxhcbxdnibxbubwnpvuxanq/Build/Products/AdHoc Testing-iphonesimulator/Virtual Pharmacist.app'
                    }

    context.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    # context.driver.switch_to_alert().accept()


def before_feature(context, feature):
    time.sleep(THINK_TIME)
    pass


def before_scenario(context, scenario):
    pass


def after_scenario(context, scenario):
    pass


def after_step(context, step):
    pass


def after_feature(context, feature):
    if context.config.userdata.get('test_type') == "selenium":
        context.browser.quit()


def tearDown(context, device_ID):
    if context.driver is not None:
        context.driver.quit()
    pass


def after_all(context):
    # TODO: For every launched appium server there should be a tear down
    # context.driver.quit()
    pass
