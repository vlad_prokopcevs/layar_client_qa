# Created by searcher at 25/06/15
@skip
Feature: Displaying introduction screen on a first launch
  I BELIEVE THAT VP user
  SHOULD BE shown a logo AND short app description AND Terms and Conditions
  AND should agree to Terms and Conditions before using the app
  SO THAT user is made aware of terms and conditions and confirms that he is aware of them.

  AND TO VERIFY THIS:

#  Background:
#    Given VP app is running for the first time
#    Given: Application is launched for the first time

  @skip
  Scenario: Make sure an intro screen is shown when app is launched for the first time
    Given VP app is running for the first time
    When User looks on the screen
    Then User sees the intro screen

  @skip
  Scenario: Logo and short description are present
    When User sees the intro screen
    Then Logo is shown
    And Following short description is shown
      """
      You have been personally selected to download this App. The content you see will be tailored to your personal needs. Scan the PhArmacist image every month & bring him to life to hear what new support is available to you. Over the 6 months we will keep track of your progress for you to review in a chart at the end.
      """

  @skip
  Scenario: Terms and Conditions link is present
    When User sees the intro screen
    Then User sees the Terms and Conditions link


  @skip
  Scenario: Tapping on the "Terms and Conditions" link opens Terms and Conditions page
    Given User is on the intro screen
    When User taps on the "Terms and Conditions" link
    Then User is redirected to "Terms and Conditions" activity

  @skip
  Scenario: After reviewing "Terms and Conditions" user can return to the intro screen
    Given User is on the "Terms and Conditions" activity
    When User closes "Terms and Conditions" activity
    Then User sees the intro screen

  @skip
  Scenario: Checkbox to accept terms and conditions is present
    When User sees the intro screen
    Then User sees Checkbox to accept terms and conditions

  @skip
  Scenario: Button to start the app for the first time is named "Let's Get Started!"
    When User sees the intro screen
    Then User sees Button named "Let's Get Started!"

  @skip
  Scenario: User cant use the app before he accepts terms and conditions
    Given User is on the intro screen
    When "Accept Terms and Conditions" checkbox is empty
    Then "Let's Get Started" button is inactive
    And Tapping "Let's Get Started" button does not bring user to the next screen

  @skip
  Scenario: User can use the app when he accepts the terms and conditions
    Given User is on the intro screen
    And Taps the "Accept Terms and Conditions" checkbox
    When "Accept Terms and Conditions" checkbox is full
    Then "Let's Get Started" button is active
    And Tapping "Let's Get Started" button brings user to the next screen


