# Created by searcher at 26/08/15
Feature: Launching the Layar application
  I BELIEVE that user
  SHOULD BE able to launch the Layar app
  SO THAT he would be able to interact with it
  AND TO VERIFY THAT...

Background:
  Given Endpoint is "testing.layar.com"
  And User is logged in as "vlad_test" with a password "test123"

  @Skip
  Scenario: Launch Layar App
    Given the device is unlocked
    And the Layar app is installed
    And user is is not running any other apps
      When User taps on the app icon
      Then Layar app is launched

  @Skip
  Scenario: Scene is recognised
    Given User is logged in as developer
    And Video "20150828_154941_90.mp4" is loaded
    When User taps on the screen
    Then Scene "OnePlus by Vlad" is recognised

  Scenario: Simple Website Link
    Given Video "001_Simple_website_2_1.mp4" is loaded
    When User taps on the screen
    Then Scene "Simple Site Button" is recognised
    And "Green" colour "/Users/searcher/Documents/BitBucket/layar_client_qa/features/testcases/images_to_search/clean_green_button.jpg" control with label "Layar Website Link" is shown
    And Tapping "/Users/searcher/Documents/BitBucket/layar_client_qa/features/testcases/images_to_search/clean_green_button.jpg" opens a web page "http://www.layar.com/"