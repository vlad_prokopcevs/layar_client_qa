from selenium.common.exceptions import NoSuchElementException

__author__ = 'searcher'

from appium import webdriver


class SideMenu:

    def __init__(self, driver):
        """
        @type driver: Remote
        """
        self.driver = driver

    @property
    def is_side_menu(self):
        try:
            self.driver.find_element_by_name("Recent content").is_displayed()
            return True
        except NoSuchElementException:
            return False

    def open_settings(self):
        if self.driver.capabilities.get("platformName") == 'Android':  # Android
            self.driver.find_element_by_name("User settings").click()
        else:  # iOS
            self.driver.find_element_by_name("Settings").click()

    def hide_side_menu(self):
        if self.is_side_menu:
            self.driver.find_element_by_name("Menu").click()
