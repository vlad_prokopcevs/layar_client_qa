from selenium.common.exceptions import NoSuchElementException, NoSuchAttributeException
import sys
from appium.webdriver import Remote

__author__ = 'searcher'


class EndpointPage:
    def __init__(self, driver):
        """
        @type driver: Remote
        """
        self.driver = driver

    @property
    def is_endpoint_page(self):
        try:
            # print (str(self.driver.page_source))
            if self.driver.find_element_by_name("Endpoint").tag_name == u'UIANavigationBar':
                return True
            else:
                return False
        except (NoSuchAttributeException, NoSuchElementException):
            return False
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise

    def select_endpoint(self, endpoint):
        self.driver.find_element_by_name(endpoint).click()

    def quit_endpoint_page(self):
        self.driver.find_element_by_name(" ").click()
