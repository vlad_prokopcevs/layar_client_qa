
__author__ = 'searcher'

from selenium.common.exceptions import NoSuchAttributeException, NoSuchElementException
import time
from selenium.webdriver.common.by import By
from features.globvar import THINK_TIME
from appium.webdriver import Remote

# LOGIN_FIELD = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIATextField[1]"
# PASSWORD_FIELD = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[2]/UIASecureTextField[1]"
ANDROID_LOGIN_FIELD_TUPLE_NAME = (By.NAME, "Username")
# ANDROID_PASSWORD_FIELD_TUPLE_NAME = (By.XPATH, "//android.view.View[1]/android.widget.FrameLayout[2]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.EditText[2]")
ANDROID_PASSWORD_FIELD_TUPLE_NAME = (By.XPATH, "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.FrameLayout[2]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.EditText[2]")
# iOS_LOGIN_FIELD_TUPLE_NAME = (By.NAME, "Username or email")
iOS_LOGIN_FIELD_TUPLE_NAME = (By.XPATH, "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIATextField[1]")
# iOS_PASSWORD_FIELD_TUPLE_NAME = (By.NAME, "password_field")
iOS_PASSWORD_FIELD_TUPLE_NAME = (By.XPATH, "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[2]/UIASecureTextField[1]")
# "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[2]/UIASecureTextField[1]"
ANDROID_NAVIGATE_UP_TUPLE_NAME = (By.NAME, "Navigate up")
iOS_NAVIGATE_UP_TUPLE_NAME = (By.NAME, "Back")


class SettingsAccountPage:
    def __init__(self, driver):
        """
        @type driver: Remote
        """
        self.driver = driver

    @property
    def is_account_page(self):
        if self.driver.capabilities.get("platformName") == 'Android':  # Android
            try:
                if self.driver.find_element_by_name("Account").tag_name == u'android.widget.TextView':
                    return True
                else:
                    return False
            except (NoSuchAttributeException, NoSuchElementException):
                return False
        else:  # iOS
            try:
                if self.driver.find_element_by_name("Account").tag_name == u'UIANavigationBar':
                    return True
                else:
                    return False
            except (NoSuchAttributeException, NoSuchElementException):
                return False

    def enter_username(self, username):
        time.sleep(THINK_TIME)
        self.login_field().send_keys(username)

    def login_field(self):
        if self.driver.capabilities.get("platformName") == 'Android':  # Android
            try:
                return self.driver.find_element(*ANDROID_LOGIN_FIELD_TUPLE_NAME)
            except (NoSuchAttributeException, NoSuchElementException):
                print("Could not enter username")
                raise
                pass
        else:  # iOS
            try:
                return self.driver.find_element(*iOS_LOGIN_FIELD_TUPLE_NAME)
            except (NoSuchAttributeException, NoSuchElementException):
                print("Could not enter username")
                raise
                pass

    def enter_password(self, password):
        self.password_field().send_keys(password)

    def password_field(self):
        if self.driver.capabilities.get("platformName") == 'Android':  # Android
            return self.driver.find_element(*ANDROID_PASSWORD_FIELD_TUPLE_NAME)
        else:  # iOS
            return self.driver.find_element(*iOS_PASSWORD_FIELD_TUPLE_NAME)

    def login(self, username, password):
        self.enter_username(username)
        self.enter_password(password)
        self.driver.find_element_by_name("Log in").click()
        time.sleep(THINK_TIME)

    def navigate_up_button(self):
        if self.driver.capabilities.get("platformName") == 'Android':  # Android
            try:
                return self.driver.find_element(*ANDROID_NAVIGATE_UP_TUPLE_NAME)
            except (NoSuchAttributeException, NoSuchElementException):
                print("Could not navigate")
                raise
                pass
        else:  # iOS
            try:
                return self.driver.find_element(*iOS_NAVIGATE_UP_TUPLE_NAME)
            except (NoSuchAttributeException, NoSuchElementException):
                print("Could not navigate")
                raise
                pass

    def exit_page(self):
        self.navigate_up_button().click()
        time.sleep(THINK_TIME)

