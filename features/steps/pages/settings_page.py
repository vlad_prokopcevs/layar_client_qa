from selenium.common.exceptions import NoSuchElementException, NoSuchAttributeException
import sys
from features.steps.pages.settings_account_page import SettingsAccountPage
from appium.webdriver import Remote

__author__ = 'searcher'


class SettingsPage:
    def __init__(self, driver):
        """
        @type driver: Remote
        """
        self.driver = driver

    @property
    def is_settings_page(self):
        if self.driver.capabilities.get("platformName") == 'Android':  # Android
            try:
                # print (str(self.driver.page_source))
                if self.driver.find_element_by_name("User settings").tag_name == u'android.widget.TextView':
                    return True
                else:
                    return False
            except (NoSuchAttributeException, NoSuchElementException):
                return False
            except:
                print "Unexpected error:", sys.exc_info()[0]
                raise
        else:  # iOS
            try:
                # print (str(self.driver.page_source))
                if self.driver.find_element_by_name("Settings").tag_name == u'UIANavigationBar':
                    return True
                else:
                    return False
            except (NoSuchAttributeException, NoSuchElementException):
                return False
            except:
                print "Unexpected error:", sys.exc_info()[0]
                raise


    @property
    def is_logged_in(self):
        try:
            self.driver.find_element_by_name("Log out").is_displayed()
            return True
        except NoSuchElementException:
            return False

    def is_logged_in_as(self, user_name):
        """
        @param user_name: str
        @return: bool
        """
        try:
            self.driver.find_element_by_name(user_name).is_displayed()
            return True
        except NoSuchElementException:
            return False

    @property
    def is_logged_in_as_dev(self):
        try:
            self.driver.find_element_by_name("Developer options").is_displayed()
            return True
        except NoSuchElementException:
            return False

    def log_out(self):
        self.driver.find_element_by_name("Log out").click()

    def login(self, username, password):
        self.driver.find_element_by_name("Account").click()
        account_page = SettingsAccountPage(self.driver)
        if not account_page.is_account_page:
            print ("Could not open login page.")
            sys.exit(0)
        account_page.login(username, password)
        if self.is_logged_in:
            print("Logged in with credentials '{}:{}'".format(username, password))
        else:
            print("Could not login with credentials '{}:{}'".format(username, password))
            account_page.exit_page()
            # sys.exit(0)

    def open_developer_options(self):
        self.driver.find_element_by_name("Developer options").click()

    def quit_settings_page(self):
        self.driver.find_element_by_name(" ").click()

    def open_endpoint_page(self):
        """
        @param endpoint  str
        """
        try:
            endpoint_button = self.driver.find_element_by_name("Endpoint: dev.layar.com")
            endpoint_button.click()
        except (NoSuchAttributeException, NoSuchElementException):
            print ("Endpoint: dev.layar.com was not found. Checking if Endpoint: testing.layar.com is present")
            try:
                endpoint_button = self.driver.find_element_by_name("Endpoint: testing.layar.com")
                endpoint_button.click()
            except (NoSuchAttributeException, NoSuchElementException):
                print ("Could not find any known Endpoints")
                raise
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise

    def is_endpoint(self, endpoint):
        try:
            self.driver.find_element_by_name("Endpoint: " + endpoint)
            return True
        except (NoSuchAttributeException, NoSuchElementException):
            print ("Could not find endpoint {}. Attempting to change current endpoint to given.".format(endpoint))
            return False
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise

    def accept_endpoint_change(self):
        try:
            ok_button = self.driver.find_element_by_name("OK")
            ok_button.click()
        except (NoSuchAttributeException, NoSuchElementException):
            print ("Could not confirm endpoint change. Skipping.")
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise
