__author__ = 'searcher'

from appium.webdriver import Remote
from selenium.common.exceptions import NoSuchElementException, NoSuchAttributeException
import sys
import features.globvar
FILE_PATH_FIELD = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[4]/UIATextField[1]"
# SERVER_DIR_PATH = "http://teamcity.local/videos/"
SERVER_DIR_PATH = "http://custom.layar.nl/~vlad/video/"


class DevOptionsPage:
    def __init__(self, driver):
        """
        @type driver: Remote
        """
        self.driver = driver

    @property
    def is_dev_options_page(self):
        try:
            # print (str(self.driver.page_source))
            if self.driver.find_element_by_name("Developer options").tag_name == u'UIANavigationBar':
                return True
        except (NoSuchAttributeException, NoSuchElementException):
            return False
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise

    def use_custom_video(self, file_name):
        """
        @param file_name: str
        @return:
        """
        # custom_video_switch = self.driver.find_elements_by_name("Use Custom Video")[2]
        custom_video_switch = self.driver.find_elements_by_class_name("UIASwitch")[1]
        if custom_video_switch.get_attribute("value") == 0:
            custom_video_switch.click()
        text_fld_file_path = self.driver.find_element_by_xpath(FILE_PATH_FIELD)
        text_fld_file_path.clear()
        # code to use smaller videos for weaker devices. Skipping for now, as there is no smaller video I can use
        # if self.driver.capabilities.get("udid") in features.globvar.SMALL_DEVICES:
        #     index = file_name.find(".mp4")
        #     file_name = file_name[:index] + "_small" + file_name[index:]
        str_full_file_path = "{0}{1}".format(SERVER_DIR_PATH, file_name)
        # text_fld_file_path.send_keys(str_full_file_path)
        self.driver.set_value(text_fld_file_path, str_full_file_path)
        # execute_script %(au.getElement('#{ref}').setValue('#{text}');)
        loop_video_switch = self.driver.find_elements_by_class_name("UIASwitch")[2]
        if loop_video_switch.get_attribute("value") == 0:
            loop_video_switch.click()

    def quit_dev_options_page(self):
        self.driver.find_element_by_name(" ").click()
