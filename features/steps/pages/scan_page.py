from features.image_recognition.image_finder import ImageFindException

__author__ = 'searcher'

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from appium.webdriver import Remote
from selenium.common.exceptions import NoSuchElementException
from features.globvar import THINK_TIME, SLOW_DEVICES
from features.image_recognition.image_recogniyion_pack import find_image, tap_image

UIA_BUTTON_XPATH = "//UIAApplication[1]/UIAWindow[1]/UIAButton[21]"
UNRECOGNISED_SCENE_BUTTON_TUPLE = (By.NAME, "App Icon Button")
SCENE_ICON_BUTTON_TUPLE = (By.NAME, "App Branding icon")
# ANDROID_TAP_SCREEN_TUPLE = (By.XPATH, "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]")
ANDROID_TAP_SCREEN_TUPLE = (By.NAME, "Tap screen to scan")
# ANDROID_TAP_SCREEN_TUPLE = (By.CLASS_NAME, "android.widget.TextView")
# ANDROID_MENU_BUTTON_TUPLE = (By.XPATH, "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.ImageView[1]")
ANDROID_MENU_BUTTON_TUPLE = (By.XPATH, "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.ImageView[1]")


class ScanPage:
    def __init__(self, driver):
        """
        @type driver Remote
        """
        self.driver = driver

    @property
    def is_scan_page(self):
        # self.driver.execute_script("mobile: tap", {"touchCount": "1", "x": "0.9", "y": "0.8", "element": self.driver.find_element_by_name("tap screen to scan").id})
        # self.driver.execute_script("mobile: tap", {"touchCount": "1", "x": "0.9", "y": "0.8",
        #                                            "element": self.driver.find_element_by_xpath(
        #                                                "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.CheckBox[1]").id})
        # self.driver.execute_script(
        #     "function postToURL(url, values) {" +
        #     "values = values || {};"+
        #     'var form = createElement("form", {action: url,' +
        #                               'method: "POST",' +
        #                               'style: "display: none"});' +
        #     'for (var property in values) {' +
        #     'if (values.hasOwnProperty(property)) {' +
        #     'var value = values[property];' +
        #     'if (value instanceof Array) {' +
        #         'for (var i = 0, l = value.length; i < l; i++) {' +
        #             'form.appendChild(createElement("input", {type: "hidden",' +
        #                                                      'name: property,' +
        #                                                      'value: value[i]}));' +
        #         '}'+
        #     '}'+
        #     'else {' +
        #         'form.appendChild(createElement("input", {type: "hidden",' +
        #                                                  'name: property,' +
        #                                                  'value: value}));' +
        #             '}' +
        #         '}' +
        #     '}' +
        #     'document.body.appendChild(form);' +
        #     'form.submit();' +
        #     'document.body.removeChild(form);'+
        #     '}',
        #     ("http://example.com/", "a")
        # )
        if self.driver.capabilities.get("platformName") == 'Android':  # Android
            try:
                self.driver.find_element(*ANDROID_TAP_SCREEN_TUPLE).is_displayed()
                return True
            except NoSuchElementException:
                return False
        else:  # iOS
            try:
                self.driver.find_element_by_name("tap screen to scan").is_displayed()
                return True
            except NoSuchElementException:
                return False

    def open_menu(self):
        if self.driver.capabilities.get("platformName") == 'Android':  # Android
            try:
                if not self.driver.find_element_by_name("User settings").is_displayed():
                    self.driver.find_element(*ANDROID_MENU_BUTTON_TUPLE).click()
            except NoSuchElementException:
                self.driver.find_element(*ANDROID_MENU_BUTTON_TUPLE).click()
        else:  # iOS
            try:
                if not self.driver.find_element_by_name("Settings").is_displayed():
                    self.driver.find_element_by_name("Menu Icon").click()
            except NoSuchElementException:
                self.driver.find_element_by_name("Menu Icon").click()

    def scan(self):
        # if self.is_scan_page:
        self.driver.find_element_by_name("Scan Screen Button").click()

    def is_scene_recognised(self, scene_name):
        """
        @param scene_name str
        @return bool
        """
        # time.sleep(THINK_TIME)
        if self.driver.capabilities.get("udid") in SLOW_DEVICES:
            # it takes time to download video on old devices. on iPad 2 I need to wait for at least 20 seconds.
            time.sleep(30)
        else:
            time.sleep(THINK_TIME * 3)  # at least 6 sec pause
        # click on the scene icon to open it
        try:
            self.driver.find_element(*SCENE_ICON_BUTTON_TUPLE).click()
            # wait = WebDriverWait(self.driver, 10)
            # scene_icon = wait.until(EC.element_to_be_clickable(*SCENE_ICON_BUTTON_TUPLE))
            # scene_icon.click()
        except NoSuchElementException:
            return False
        # check scene name
        try:
            if self.driver.find_element_by_name(scene_name).is_displayed():
                # if the scene name is found, close the scene description
                try:
                    self.driver.find_element(*SCENE_ICON_BUTTON_TUPLE).click()
                    print("Scene name was found and it is displayed")
                    return True
                except NoSuchElementException:
                    print("Could not close the scene description. Something is wrong")
                    return False
            else:
                print("Scene name was found, but it is not visible. Something is wrong.")
                return False
        except NoSuchElementException:
            print("We could not even locate scene description")
            return False

    def is_control_image_found(self, image):
        try:
            top_left, bottom_right = find_image(self.driver, image)
            print (
                'Image coordinates:\n'
                ' - Top left corner: {},\n'
                ' - Bottom right corner: {}'.format(top_left, bottom_right))
            return True
        except ImageFindException:
            print("Image was not found")
            return False

    def tap_control(self, control):
        try:
            tap_image(self.driver, control)
        except ImageFindException:
            print("Control was not found")
            raise

    def is_webpage_displayed(self, web_address):
        self.driver.context()
        print (self.driver.current_url)
        print (self.driver.page_source)

        pass
