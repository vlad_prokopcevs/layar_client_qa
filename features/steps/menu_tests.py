from behave import *
from features.steps.pages.scan_page import ScanPage
# use_step_matcher("re")
from features.steps.pages.settings_page import SettingsPage
from features.steps.pages.side_menu import SideMenu


@given("User is on the settings page")
def step_impl(context):
    """
    @type context: behave.runner.Context
    """
    wd = context.driver
    scan_page = ScanPage(wd)
    if scan_page.is_scan_page:
        scan_page.open_menu()
    side_menu = SideMenu(wd)
    if side_menu.is_side_menu:
        side_menu.open_settings()
    settings_page = SettingsPage(wd)
    if not settings_page.is_settings_page:
        assert False, "Could not open settings."
    pass


@when('User logs in as "{user_name}" with a password "{password}"')
def step_impl(context, user_name, password):
    """
    @param password:
    @param user_name:
    @type context: behave.runner.Context
    """
    wd = context.driver
    settings_page = SettingsPage(wd)
    if user_name == "[None]":
        user_name = ""
    if password == "[None]":
        password = ""
    if not settings_page.is_settings_page:
        assert False, "Could not open settings."
    settings_page.login(user_name, password)
    pass


@then("User is authenticated {auth_result}")
def step_impl(context, auth_result):
    """
    @param auth_result:
    @type context: behave.runner.Context
    """
    expected_logged_in = True if "successfully" == auth_result else False
    wd = context.driver
    settings_page = SettingsPage(wd)
    if not settings_page.is_settings_page:
        assert False, "We are not on settings page."
    assert (settings_page.is_logged_in == expected_logged_in), "Unexpected authentication result. Expected: {}".format(expected_logged_in)
    pass


@given("User is not logged in")
def step_impl(context):
    """
    @type context: behave.runner.Context
    """
    wd = context.driver
    settings_page = SettingsPage(wd)
    if settings_page.is_logged_in:
        settings_page.log_out()
    pass
