# ALL THE SCENARIO FILES MUST CONTAIN A SIMILAR STRUCTURE, MUST BE .py FILES

# from selenium.webdriver.common.by import By
# ...
from behave import *
from features.steps.pages.endpoint_page import EndpointPage
from features.steps.pages.scan_page import ScanPage
from features.steps.pages.settings_devOptions_page import DevOptionsPage
from features.steps.pages.settings_page import SettingsPage
from features.steps.pages.side_menu import SideMenu
import time
from appium import webdriver
from features.globvar import *
from hamcrest import assert_that, equal_to
from parse import *


# context.driver = webdriver.Remote('http://localhost:' + str(port) + '/wd/hub', desired_caps)

# use_step_matcher("re")


############################
# ## CSS SELECTOR VARIABLES
############################

# USERNAME_INPUT = """#user_input"""
# LOGIN_BUTTON = """.btn"""
# ....



############################
# ## HELPER METHODS
############################

# def parse_usernames(context, list):
#     pass
# .....


############################
# ## GIVEN STEPS
############################

# @given('a user with name {name} logged in')
# def step(context, user):
#    pass



@given("the Layar app is installed")
def step_impl(context):
    """
    :type context behave.runner.Context
    """

    pass


@step("user is is not running any other apps")
def step_impl(context):
    """
    :type context behave.runner.Context
    """
    pass


@given('User is logged in as developer')
def step_impl(context):
    """
    :type context behave.runner.Context
    """
    # vlad\.prokopcevs@blippar\.com
    # context.driver.find_element_by_name("tap screen to scan")
    # context.driver.
    wd = context.driver
    scan_page = ScanPage(wd)
    if not scan_page.is_scan_page:
        assert False, "Current page is not scan page."
    scan_page.open_menu()
    side_menu = SideMenu(wd)
    if not side_menu.is_side_menu:
        assert False, "Could not open side menu."
    side_menu.open_settings()
    settings_page = SettingsPage(wd)
    if not settings_page.is_settings_page:
        assert False, "Could not open settings."
    if not settings_page.is_logged_in:
        settings_page.login("vlad.prokopcevs@blippar.com", "1q2w3e4r")
    else:
        if not settings_page.is_logged_in_as_dev:
            assert False, "User has no developer rights"
    pass


@given('Video "{file_name}" is loaded')  # "20150826_153708_90\.mp4"
def step_impl(context, file_name):
    """
    @type context behave.runner.Context
    @type file_name str
    """
    wd = context.driver
    scan_page = ScanPage(wd)
    side_menu = SideMenu(wd)
    settings_page = SettingsPage(wd)
    if not settings_page.is_settings_page:
        if not side_menu.is_side_menu:
            if not scan_page.is_scan_page:
                assert False, "Could not open settings. Navigation error"
            else:
                scan_page.open_menu()
                side_menu.open_settings()
        else:
            side_menu.open_settings()
    settings_page.open_developer_options()
    dev_opt_page = DevOptionsPage(wd)
    if not dev_opt_page.is_dev_options_page:
        assert False, "Could not open Developer Options."
    dev_opt_page.use_custom_video(file_name)
    dev_opt_page.quit_dev_options_page()
    settings_page.quit_settings_page()
    side_menu = SideMenu(wd)
    side_menu.hide_side_menu()
    if context.driver.capabilities.get("udid") in SLOW_DEVICES:
        # it takes time to download video on old devices. on iPad 2 I need to wait for at least 20 seconds.
        time.sleep(20)
    else:
        time.sleep(THINK_TIME * 3)  # at least 6 sec pause
    pass


@given('Endpoint is "{endpoint}"')  # testing\.layar\.com
def step_impl(context, endpoint):
    """
    @type context behave.runner.Context
    @type endpoint str
    """
    wd = context.driver
    scan_page = ScanPage(wd)
    if not scan_page.is_scan_page:
        assert False, "Current page is not scan page."
    scan_page.open_menu()
    side_menu = SideMenu(wd)
    if not side_menu.is_side_menu:
        assert False, "Could not open side menu."
    side_menu.open_settings()
    settings_page = SettingsPage(wd)
    if not settings_page.is_settings_page:
        assert False, "Could not open settings."
    if not settings_page.is_endpoint(endpoint):
        settings_page.open_endpoint_page()
        endpoint_page = EndpointPage(wd)
        if not endpoint_page.is_endpoint_page:
            assert False, "Could not open Endpoint settings."
        endpoint_page.select_endpoint(endpoint)
        endpoint_page.quit_endpoint_page()
        time.sleep(THINK_TIME)
        settings_page.accept_endpoint_change()
    settings_page.quit_settings_page()


@given("the device is unlocked")
def step_impl(context):
    """
    :type context behave.runner.Context
    """
    pass


@given('User is logged in as "{user_name}" with a password "{password}"')
def step_impl(context, user_name, password):
    """
    @type context behave.runner.Context
    """

    wd = context.driver
    scan_page = ScanPage(wd)
    if not scan_page.is_scan_page:
        assert False, "Current page is not scan page."
    scan_page.open_menu()
    side_menu = SideMenu(wd)
    if not side_menu.is_side_menu:
        assert False, "Could not open side menu."
    side_menu.open_settings()
    settings_page = SettingsPage(wd)
    if not settings_page.is_settings_page:
        assert False, "Could not open settings."
    if not settings_page.is_logged_in_as(user_name):
        settings_page.login(user_name, password)
    else:
        if not settings_page.is_logged_in_as_dev:
            assert False, "User has no developer rights"
    settings_page.quit_settings_page()
    pass


############################
# ## WHEN STEPS
############################

# @when('I set the user {user} in login')
# def step(context, user):
#    pass


@when("User taps on the screen")
def step_impl(context):
    """
    @type context behave.runner.Context
    """
    wd = context.driver
    scan_page = ScanPage(wd)
    if not scan_page.is_scan_page:
        assert False, "Current page is not scan page."
    scan_page.scan()
    time.sleep(THINK_TIME)
    pass


@when("User taps on the app icon")
def step_impl(context):
    """
    :type context behave.runner.Context
    """
    pass


############################
# ## THEN STEPS
############################

# @then('The {user} is logged in')
# def step(context, user):
#    pass

@then('Scene "{scene_name}" is recognised')
def step_impl(context, scene_name):
    """
    @type context behave.runner.Context
    @type scene_name str
    """
    # print (str(context.driver.page_source)).encode('ascii', 'ignore')
    wd = context.driver
    scan_page = ScanPage(wd)
    if not scan_page.is_scan_page:
        assert False, "Current page is not scan page."
    if not scan_page.is_scene_recognised(scene_name):
        assert False, "Could not recognise the scene"
    pass


@then("Layar app is launched")
def step_impl(context):
    """
    :type context behave.runner.Context
    """
    pass


@step('{colour} colour "{control}" control with label "{label}" is shown')
def step_impl(context, colour, control, label):
    """
    @type context behave.runner.Context
    @type label str
    @param control: image of the control to search for
    """
    wd = context.driver
    scan_page = ScanPage(wd)
    # print (str(context.driver.page_source)).encode('ascii', 'ignore')

    assert scan_page.is_control_image_found(control), "Could not find given control."

    # scan_page.is_element_present_by_name
    pass


@step('Tapping "{control}" opens a web page "{web_address}"')  # http://www\.layar\.com/
def step_impl(context, control, web_address):
    """
    @type context behave.runner.Context
    @type web_address str
    @param web_address: webpage address to verify against
    """

    wd = context.driver
    scan_page = ScanPage(wd)
    # print (str(context.driver.page_source)).encode('ascii', 'ignore')

    scan_page.tap_control(control)


    assert scan_page.is_webpage_displayed(web_address), "Could not find given control."

    pass
