# ALL THE SCENARIO FILES MUST CONTAIN A SIMILAR STRUCTURE, MUST BE .py FILES

# from selenium.webdriver.common.by import By
# ...


############################
# ## CSS SELECTOR VARIABLES
############################

# USERNAME_INPUT = """#user_input"""
# LOGIN_BUTTON = """.btn"""
# ....



############################
# ## HELPER METHODS
############################

# def parse_usernames(context, list):
#     pass
# .....


############################
# ## GIVEN STEPS
############################

# @given('a user with name {name} logged in')
# def step(context, user):
#    pass


############################
# ## WHEN STEPS
############################

# @when('I set the user {user} in login')
# def step(context, user):
#    pass

############################
# ## THEN STEPS
############################

# @then('The {user} is logged in')
# def step(context, user):
#    pass
