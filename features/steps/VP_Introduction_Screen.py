from behave import *
import time
from appium import webdriver
from features.globvar import *
from hamcrest import assert_that, equal_to
from parse import *

# use_step_matcher("re")

#######################
#### HELPER METHODS####
#######################
def current_screen_terms(context):
    # make sure current screen is the initial screen (with link to terms and conditions)
    # assert False, context.driver.current_activity
    assert (
        context.driver.current_activity == 'com.lloyds.pharmacist.TermsConditionsActivity'), \
        "Error, current activity is wrong: {activity}".format(
            activity=context.driver.current_activity)
    pass


#####
# Scenario: Make sure an intro screen is shown when app is launched for the first time
#####
@given("VP app is running for the first time")
def step_impl(context):
    time.sleep(THINK_TIME)
    # print (context.config.userdata["device_id"])
    assert context.driver.is_app_installed('com.lloyds.pharmassist') is True
    # assert context.driver.is_app_installed('com.layar.talkingmedicines') is True
    context.driver.reset()
    pass


@when("User looks on the screen")
def step_impl(context):
    """
    :type context behave.runner.Context
    """
    pass


@then("User sees the intro screen")
def step_impl(context):
    time.sleep(THINK_TIME)
    current_screen_terms(context)


###
#   Scenario: Logo and short description are present
###


@given("User is on the intro screen")
def step_impl(context):
    # time.sleep(THINK_TIME)
    current_screen_terms(context)
    pass


@when("User sees the intro screen")
def step_impl(context):
    # time.sleep(THINK_TIME)
    current_screen_terms(context)
    pass


@when("Application is loaded")
def step_impl(context):
    """
    :type context behave.runner.Context
    """
    pass


@then("Logo is shown")
def step_impl(context):
    # print (context.config.userdata["device_id"])

    # find app logo on the initial screen
    logo = context.driver.find_element_by_xpath(
        "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.ImageView[1]")
    # assert False, "logo text is: {text}".format(text=logo.get_attribute("focused"))
    # check, that located element is a web element
    assert isinstance(logo, webdriver.WebElement), \
        "Error, logo is not a WebElement. Instead logo is: {element}".format(
            element=type(logo))
    # is logo displayed?
    assert logo.is_displayed, \
        "Error, logo is not displayed. Instead logo is: {element}".format(
            element=logo.is_displayed)
    # Assert logo is not missing at all
    assert (
        logo is not None), \
        "Error, logo is missing: {element}".format(
            element=logo)
    # Test
    pass


@step("Following short description is shown")
def step_impl(context):
    # Find textbox with text
    intrText = context.driver.find_element_by_xpath(
        "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]")
    # Assert long introduction text is not missing at all
    assert (
        intrText is not None), \
        "Error, logo is missing: {element}".format(
            element=intrText)
    # assert False, "long text is: {text}".format(text=intrText.text)
    assert (
        intrText.text == context.text), \
        "Error, fond text is: {longText}".format(
            longText=intrText.text)
    pass


###
#
###



@when("\"Accept Terms and Conditions\" checkbox is {box_value}")
def step_impl(context, box_value):
    # print (context.config.userdata["device_id"])

    # Determine what state are we looking for
    if box_value.lower() in ['true', 'full', '1', 'enabled']:
        bool_state = "true"
    elif box_value.lower() in ['false', 'empty', '0', 'disabled']:
        bool_state = "false"
    else:
        raise Exception("unexpected checkbox value to verify against: '{text}'".format(text=box_value))
    # Find Checkbox
    checkbox = context.driver.find_element_by_xpath(
        "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.CheckBox[1]")
    # Assert checkbox is not missing at all
    assert (
        checkbox is not None), \
        "Error, logo is missing: {element}".format(
            element=checkbox)
    # assert checkbox value
    assert_that(checkbox.get_attribute("checked"), equal_to(bool_state))
    pass


@then('"Let\'s Get Started" button is {button_state}')
def step_impl(context, button_state):
    # Determine what state are we looking for
    if button_state.lower() in ['true', 'full', '1', 'active']:
        bool_state = "true"
    elif button_state.lower() in ['false', 'empty', '0', 'inactive']:
        bool_state = "false"
    else:
        raise Exception("unexpected button state to verify against: '{text}'".format(text=button_state))
    # Find button
    button_start = context.driver.find_element_by_xpath(
        "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.Button[1]")
    # Assert button is not missing at all
    assert (
        button_start is not None), \
        "Error, logo is missing: {element}".format(
            element=button_start)
    # assert button state
    assert_that(button_start.get_attribute("enabled"), equal_to(bool_state), "Button state assertion")
    assert_that(button_start.get_attribute("enabled"), equal_to(bool_state), "Button state assertion")
    pass


@step('Tapping "Let\'s Get Started" button does not bring user to the next screen')
def step_impl(context):
    # tap the button "Let's Get Started"
    context.driver.find_element_by_xpath(
        "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.Button[1]").click()
    time.sleep(THINK_TIME)
    current_screen_terms(context)
    pass


@step('Tapping "Let\'s Get Started" button brings user to the next screen')
def step_impl(context):
    # make sure current screen is the second screen (with code entry)
    # assert False, context.driver.current_activity
    context.driver.find_element_by_xpath(
        "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.Button[1]").click()
    time.sleep(THINK_TIME)
    assert (
        context.driver.current_activity == 'com.lloyds.pharmacist.GetStartedActivity'), \
        "Error, current activity is wrong: {activity}".format(
            activity=context.driver.current_activity)
    pass


@step('Taps the "Accept Terms and Conditions" checkbox')
def step_impl(context):
    context.driver.find_element_by_xpath(
        "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.CheckBox[1]").click()
    pass

    # if __name__ == '__main__':
    #     driver = webdriver.Remote()


@then("User sees the Terms and Conditions link")
def step_impl(context):
    """
    :type context behave.runner.Context
    """
    pass


@when('User taps on the "Terms and Conditions" link')
def step_impl(context):
    """
    :type context behave.runner.Context
    """
    pass


@then('User is redirected to "Terms and Conditions" activity')
def step_impl(context):
    """
    :type context behave.runner.Context
    """
    pass


@given('User is on the "Terms and Conditions" activity')
def step_impl(context):
    """
    :type context behave.runner.Context
    """
    pass


@when('User closes "Terms and Conditions" activity')
def step_impl(context):
    """
    :type context behave.runner.Context
    """
    pass


@then("User sees Checkbox to accept terms and conditions")
def step_impl(context):
    """
    :type context behave.runner.Context
    """
    pass


@then('User sees Button named "Let\'s Get Started!"')
def step_impl(context):
    """
    :type context behave.runner.Context
    """
    pass