# Created by searcher at 13/11/15
Feature: User Authentication
  I BELIEVE that app user
  SHOULD be able to log into the app
  TO GAIN access to advanced features
  AND TO VERIFY THIS ...

  Scenario Outline: Login menu item
   Given User is on the settings page
    And User is not logged in
    When User logs in as "<user_name>" with a password "<password>"
    Then User is authenticated <result>

    Examples:
      | user_name | password | result         |
      | vlad_live | test123  | successfully   |
      | [None]    | [None]   | unsuccessfully |
