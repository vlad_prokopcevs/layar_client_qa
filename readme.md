# Getting started

This is the base repository for any automation project in blippar/layar using behave. This repository must not be the changed, it is maintained by `vlad.prokopcevs@blippar.com`.
The intention of this is to have an starting structure of any automation project. This readme will explain how to start a new project.


## Set up your environment

 - Create a new repo in bitbucket be sure that the owner is layar_dev. Set a name and description and as language python.
 - Create the local repo from the base behave in your computer: `hg clone base_behave {destination_dir}`
 - Enter in the directory where the repo was cloned, `cd .hg` and edit the file `hgrc`. Change the `default` section of `[paths]` and change it to point it to the new repository
 - Run `hg push` to update your new repo with the base. At this point you will have your new repo with a copy of base behave and you are ready to start working there.
 
It is recommended to use virtual environments:
 
 - First of all install python 2.7 (latest version of 2.7)
 - Install virtualenv: `pip install virtualenv`
 - Create your virtual environment using `virtualenv {name}` (this will create a directory with the virtual environment
 - Open virtual env: `source {virtual_env_dir}/bin/activate`
 - Install requirements: `pip install -r -U requirements.txt` 
To deactivate the virtualenv run `deactivate`

## Writing a test

[Full documentation here](http://pythonhosted.org/behave/tutorial.html),
quick rundown below.

A feature file is a collection of steps that are called with parameters.
Usually you will start by creating a feature file and then implementing the 
steps, but make sure you are aware of what steps already exist (logging in, 
clicking things, etc) and you should reuse those.

### Feature file

Look at an existing feature and create a new file in `features/testcases`.
A feature file "calls" multiple steps with parameters. 

 - `Feature`: Natural name of the feature, does not have to match anything. 
 - @DECORATOR: Group your feature, optional (useful for --tags option)
 - `Scenario`: Natural name of the scenario, does not have to match anything.
   Your browser will restart after every scenario.
 - The `Given`, `When` and `Then` parts must match a step from a steps file.
 - If any of them is repeated (e.g. 3 `When` in consecutive lines) then it is possible to use `And` after the 1st `When`

### Steps file

A steps file defines the actions your browser should execute.  
Look at an existing step and create a new steps file in `features/steps`. 

 - The `@given` part is a natural language string with parameters. You will
   use this in your `.feature` file and in other steps.
 - The `step` function defines a collections of steps the browser should execute.
 - `context` is always the first parameters of the `step` function. If your
   step accepts parameters (`{var}`), they should be added too.
 - The `context.execute_steps` defines a single step. You can re-use other steps
   here that are defined elsewhere. The most basic steps (opening a URL, 
   clicking things) can be found in base library.

## Running a test

NOTE: If you are using virtual envs enter your virtual env first : `source {virtual_env_dir}/bin/activate`

This is a quick command to run tests locally (firefox browser is required)
From `features` directory:
`behave "testcases/" --junit -D test_type="selenium" -D browser_name="local" -D target_env="{environment_under_test" -D wait_between_tests="2" --no-skipped`

 - The value after behave is the testcase directory, this will run all the tests there. If you want to run a specific file the file can be specified after the /
 - `{environment_under_test}` is the url of the stack were the app under test is running
 - Tip: To execute a specific scenario, it is possible to add a decorator over the scenario in the feature file (eg. `@UNDER_TEST`) and then add the parameter --tags=UNDER_TEST. This will execute all scenarios with that tag.
 

## More options to run tests (SELENIUM)

In the case that you want to run the test using a webdriver (could be your own, browserstack, saucelabs, etc..) more info must be provided. The following is a list of parameter that must be set to run tests remotely:

 - `-D webdriver_hub`: The url to the selenium webdriver
 - `-D test_type` : The type of test to run. For selenium use `selenium`
 - `-D browser_name` : The name of the browser to run the tests. Options are `local`, `firefox`, `chrome`, `ie`, `opera`, `safari`
 - `-D os`: operating system. Options are `Windows`, `OS X`
 - `-D os_version`: os version. Options for windows: `8.1`, `8`, `7`, `XP`. OSX options: `Yosemite`, `Maverics`, `Mountain Lion`, `Lion`, `Snow Leopard`
 - `-D browser_version`: Number (depends on browser). Leave empty for latest in webdriver.
 - `-D resolution`: Screen resolution. Options are: `1024x768`, `1280x960`, `1280x1024`, `1600x1200`, `1920x1080` (recommended: `1280x1024`)

## More options to run tests (API)
 - `-D test_type` : `API` must be entered to use API tests. In this case the browser or webdriver session won't be started. 

API test type is in progress. It works fine if using simple restful tests for GET, POST, PUT and DELETE.
It is probably needed to add some helpers in environment or other files that are in behave_base_library in order to support different auth. Contact `juan.moschino@blippar.com`